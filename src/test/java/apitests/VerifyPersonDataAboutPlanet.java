package apitests;

import controllers.PersonController;
import models.Film;
import models.Person;
import models.Planet;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by anna on 04.10.17.
 */
public class VerifyPersonDataAboutPlanet {
    private PersonController personController;
    private Person person;
    private Planet planet;
    private Film film;

    @Before
    public void setUp(){
        personController = new PersonController();
        person = personController.getPersonById(1);
    }

    @Test
    public void verifyThatPersonInfoAboutPlanetIsTrue(){
        String homeworld = person.getHomeworld();
        planet = personController.getHomeworldByPerson(homeworld);
        assertEquals("The name of planet is not Tatooine", "Tatooine", planet.getName());
        assertEquals("The population of planet is not 200000", "200000", planet.getPopulation());
    }

    @Test
    public void verifyThatFirstFilmHasCorrectTitle(){
        String filmUrl = person.getFilms()[0];
        film = personController.getFilmsByUrl(filmUrl);
        assertEquals("The title of film is not Attack of the Clones", "Attack of the Clones", film.getTitle());
        assertTrue(film.getCharacters()[0].contains("people/1"));
        assertTrue(film.getPlanets()[0].contains("planets/1"));
    }


}