package models;

/**
 * Created by anna on 04.10.17.
 */
public class Planet {
    private String name;
    private Integer rotation_period;
    private Integer orbital_period;
    private Integer diameter;
    private String climate;
    private String gravity;
    private String terrain;
    private Integer surface_water;
    private Integer population;
    private String[] residents;
    private String[] films;
    private String created;
    private String edited;
    private String url;

    public Planet(){}

    public Planet(String name, Integer rotation_period, Integer orbital_period, Integer diameter, String climate, String gravity, String terrain, Integer surface_water, Integer population, String[] residents, String[] films, String created, String edited, String url) {
        this.name = name;
        this.rotation_period = rotation_period;
        this.orbital_period = orbital_period;
        this.diameter = diameter;
        this.climate = climate;
        this.gravity = gravity;
        this.terrain = terrain;
        this.surface_water = surface_water;
        this.population = population;
        this.residents = residents;
        this.films = films;
        this.created = created;
        this.edited = edited;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public Integer getRotation_period() {
        return rotation_period;
    }

    public Integer getOrbital_period() {
        return orbital_period;
    }

    public Integer getDiameter() {
        return diameter;
    }

    public String getClimate() {
        return climate;
    }

    public String getGravity() {
        return gravity;
    }

    public String getTerrain() {
        return terrain;
    }

    public Integer getSurface_water() {
        return surface_water;
    }

    public Integer getPopulation() {
        return population;
    }

    public String[] getResidents() {
        return residents;
    }

    public String[] getFilms() {
        return films;
    }

    public String getCreated() {
        return created;
    }

    public String getEdited() {
        return edited;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRotation_period(Integer rotation_period) {
        this.rotation_period = rotation_period;
    }

    public void setOrbital_period(Integer orbital_period) {
        this.orbital_period = orbital_period;
    }

    public void setDiameter(Integer diameter) {
        this.diameter = diameter;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    public void setGravity(String gravity) {
        this.gravity = gravity;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    public void setSurface_water(Integer surface_water) {
        this.surface_water = surface_water;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public void setResidents(String[] residents) {
        this.residents = residents;
    }

    public void setFilms(String[] films) {
        this.films = films;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Planet{" +
                "name='" + name + '\'' +
                ", rotation_period=" + rotation_period +
                ", orbital_period=" + orbital_period +
                ", diameter=" + diameter +
                ", climate='" + climate + '\'' +
                ", gravity='" + gravity + '\'' +
                ", terrain='" + terrain + '\'' +
                ", surface_water=" + surface_water +
                ", population=" + population +
                ", residents='" + residents + '\'' +
                ", films='" + films + '\'' +
                ", created='" + created + '\'' +
                ", edited='" + edited + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
