package controllers;

import models.Film;
import models.Person;
import models.Planet;
import org.springframework.web.client.RestTemplate;

/**
 * Created by anna on 04.10.17.
 */
public class PersonController {
    private RestTemplate restTemplate;

    public PersonController(){
        restTemplate = new RestTemplate();
    }

    private final static String API_BASE = "https://swapi.co/api/";

    public Person getPersonById(Integer id){
        try {
            return restTemplate.getForObject(API_BASE + "people/" + id + "/", Person.class);
        } catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public Planet getHomeworldByPerson(String homeworld){
        try {
            return restTemplate.getForObject(homeworld, Planet.class);
        } catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public Film getFilmsByUrl(String filmUrl){
        try {
            return restTemplate.getForObject(filmUrl, Film.class);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
